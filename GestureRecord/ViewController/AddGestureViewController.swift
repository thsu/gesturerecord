//
//  AddGestureViewController.swift
//  GestureRecord
//
//  Created by Toby Hsu on 2015/2/13.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class AddGestureViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var gestureView: GestureView!
    var recognizer: DollarPGestureRecognizer!
    var strokeName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recognizer = DollarPGestureRecognizer(target: self, action: "gestureRecognized:")
        recognizer.pointClouds = DollarDefaultGestures.defaultPointClouds()
        recognizer.delaysTouchesEnded = false
        gestureView.addGestureRecognizer(recognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    
    
    @IBAction func doneAction(sender: UIBarButtonItem) {
        recognizer.recognize()
    }
    
    func gestureRecognized(sender: DollarPGestureRecognizer) {
        let result = sender.result
        println("Result: \(result.name) (Score: \(result.score))")
        gestureView.clearAll()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
