//
//  GestureListViewController.swift
//  GestureRecord
//
//  Created by Toby Hsu on 2015/2/13.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class GestureListViewController: UITableViewController {

    var records:Array<Dictionary<String, AnyObject>>! = []
    let userDefault = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let strokes = userDefault.objectForKey("strokes") as? [Dictionary<String, AnyObject>] {
            records = strokes
            println("get data from userDefault(\(strokes.count))")
        }
        else {
            userDefault.setValue([:], forKey: "strokes")
            println("first build userDefault array")
        }
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addStrokeAction(sender: UIBarButtonItem) {
        var alertController = UIAlertController(title: nil, message: "Please enter the stroke name.", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "New..."
        }
        let submitAction = UIAlertAction(title: "Submit", style: .Destructive) { (_) in
            let sourceTextField = alertController.textFields![0] as UITextField
            let stroke = ["name":sourceTextField.text, "points":[]]
            self.records.append(stroke)
            self.userDefault.setValue(self.records, forKey: "strokes")
            self.tableView.reloadData()
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        alertController.addAction(cancelAction)
        alertController.addAction(submitAction)
        
        self.presentViewController(alertController, animated: true) { () -> Void in
        }

    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return records.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        if let strokeName = records[indexPath.row]["name"] as? String {
            cell.textLabel?.text = strokeName
        }
        else {
            println("impossible to see here if no problem.")
        }
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "toAddGesture" {
            let VC = segue.destinationViewController as AddGestureViewController
            let cell = sender as UITableViewCell
            if let strokeName = cell.textLabel?.text {
                VC.strokeName = strokeName
            }
            else {
                println("no cell textLabel.")
            }
        }
    }


}
