//
//  PreviewCell.swift
//  GestureRecord
//
//  Created by Toby Hsu on 2015/2/13.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class PreviewCell: UICollectionViewCell {
    @IBOutlet var preView: UIImageView!
    
}
