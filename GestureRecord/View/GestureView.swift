//
//  GestureView.swift
//  GestureRecord
//
//  Created by Toby Hsu on 2015/2/13.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class Stroke {
    var points:Array<NSValue>?
    var color:UIColor?
    
    init(points: Array<NSValue>, inColor color:UIColor) {
        self.points = points
        self.color = color
    }
}

class GestureView: UIView {
    var currentTouches: Dictionary<NSValue,Stroke>?
    var completeStrokes: Array<Stroke>?
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        for touch in touches.allObjects as [UITouch] {
            let key = NSValue(nonretainedObject: touch)
            let location = touch.locationInView(self)
            let stroke = Stroke(points: [NSValue(CGPoint: location)], inColor: UIColor.blackColor())
            currentTouches!.updateValue(stroke, forKey: key)
        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        super.touchesMoved(touches, withEvent: event)
        for touch in touches.allObjects as [UITouch] {
            let key = NSValue(nonretainedObject: touch)
            let location = touch.locationInView(self)
            currentTouches![key]!.points!.append(NSValue(CGPoint: location))
        }
        setNeedsDisplay()
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        super.touchesEnded(touches, withEvent: event)
        endTouches(touches)
    }
    
    override func touchesCancelled(touches: NSSet!, withEvent event: UIEvent!) {
        super.touchesCancelled(touches, withEvent: event)
        endTouches(touches)
    }
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 5.0)
        CGContextSetLineCap(context, kCGLineCapRound)
        
        if let completeStrokes = completeStrokes {
            for stroke in completeStrokes {
                drawStroke(stroke: stroke, inContext: context)
            }
        }
        if let currentTouches = currentTouches {
            for touchValue in currentTouches {
                drawStroke(stroke: touchValue.1, inContext: context)
            }
        }
    }
    
    func drawStroke(#stroke: Stroke, inContext context:CGContextRef) {
        stroke.color?.set()
        let points = stroke.points
        
        for (index, value) in enumerate(stroke.points!) {
            let point = value.CGPointValue()
            if index == 0 {
                CGContextFillRect(context, CGRect(x: point.x - 5, y: point.y - 5, width: 10, height: 10))
                CGContextMoveToPoint(context, point.x, point.y)
            }
            else {
                CGContextAddLineToPoint(context, point.x, point.y)
            }
        }
        CGContextStrokePath(context)
    }
    
    func endTouches(touches: NSSet) {
        for touch in touches.allObjects as [UITouch] {
            let key = NSValue(nonretainedObject: touch)
            let stroke = currentTouches![key]!
            stroke.color = randomColor()
            
            completeStrokes?.append(stroke)
            currentTouches?.removeValueForKey(key)
        }
        setNeedsDisplay()
    }
    
    func randomColor() -> UIColor {
        let hue = CGFloat(Double(arc4random() % 256) / 256.0)
        let saturation = CGFloat(Double(arc4random() % 128) / 256.0 + 0.5)
        let brightness = CGFloat(Double(arc4random() % 128) / 256.0 + 0.5)
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    func setup() {
        currentTouches = [:]
        completeStrokes = []
    }
    func clearAll() {
        completeStrokes?.removeAll(keepCapacity: false)
        currentTouches?.removeAll(keepCapacity: false)
        setNeedsDisplay()
    }
    

}
